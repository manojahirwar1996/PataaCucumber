package stepDefinitions;

import io.cucumber.java.en.*;
import org.openqa.selenium.By;
import org.testng.Assert;
import tests.TestBase;

import java.util.concurrent.TimeUnit;

public class signupSteps extends TestBase {
    @Given("I am on pataa signup screen")
    public void i_am_on_pataa_signup_screen() throws Exception {
        // Write code here that turns the phrase above into concrete actions
        androidSetup();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @When("I enter mobile no as {string}")
    public void i_enter_mobile_no_as(String mobno) {
        // Write code here that turns the phrase above into concrete actions
      //  throw new io.cucumber.java.PendingException();
        driver.findElement(By.id("com.pataa.dev:id/editTextMobileNumber")).sendKeys(mobno);
        driver.findElement(By.id("com.pataa.dev:id/btn")).click();
    }

    @Then("OTP verification screen should display")
    public void otp_verification_screen_should_display() {
        // Write code here that turns the phrase above into concrete actions
       // throw new io.cucumber.java.PendingException();
        String Title = driver.findElement(By.id("com.pataa.dev:id/tvMessage")).getText();
        Assert.assertEquals(Title , "OTP Verification");

    }

    @When("I enter OTP as {string}")
    public void i_enter_otp_as(String otp) {
        // Write code here that turns the phrase above into concrete actions
      //  throw new io.cucumber.java.PendingException();
        driver.findElement(By.id("com.pataa.dev:id/otp_view")).sendKeys(otp);
    }

    @Then("Profile Info Screen should display")
    public void profile_info_screen_should_display() {
        // Write code here that turns the phrase above into concrete actions
     //   throw new io.cucumber.java.PendingException();
        String Title = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.TextView[1]")).getText();
        Assert.assertEquals(Title, "Profile Info");
    }

    @And("Enter First name as {string}")
    public void enterFirstNameAs(String fname){
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText")).sendKeys(fname);

        //driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);

    }

    @And("Enter Last name as {string}")
    public void enterLastNameAs(String lname)  throws Exception {
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText")).sendKeys(lname);
        driver.findElement(By.id("com.pataa.dev:id/btn")).click();
    }

    @And("Reset the App")
    public void resetTheApp() {
        resetApp();
    }
}
