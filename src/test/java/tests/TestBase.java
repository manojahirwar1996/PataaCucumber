package tests;

import ch.qos.logback.core.net.SyslogOutputStream;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.*;

import java.net.URL;


public class TestBase {


        public static AppiumDriver<MobileElement> driver;

        public static void androidSetup() throws  Exception{
            DesiredCapabilities cap = new DesiredCapabilities();
            cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "ANDROID");
            cap.setCapability(MobileCapabilityType.PLATFORM_VERSION, "11");
          cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Emulator");
          cap.setCapability(MobileCapabilityType.UDID, "emulator-5554");
//            cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Redmi Note 9");
//            cap.setCapability(MobileCapabilityType.UDID, "1b1fd07e0404");
            //  cap.setCapability(MobileCapabilityType.APP, System.getProperty("user.dir")+"apps/pataa.apk");
            cap.setCapability("appPackage", "com.pataa.dev");
            cap.setCapability("appActivity","com.pataa.ui.splash.SplashMainActivity");
            URL url= new URL("http://127.0.0.1:4723/wd/hub");
            driver = new AndroidDriver<MobileElement>(url,cap);
        }

        public  static void teardown(){
            driver.closeApp();
        }

        public static void resetApp(){
            driver.resetApp();

        }




    }


