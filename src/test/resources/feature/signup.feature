Feature: Signup
  Scenario: User can signup with valid mobile no
    Given I am on pataa signup screen
    When I enter mobile no as "9794756396"
    Then OTP verification screen should display
    When I enter OTP as "56396"
    Then Profile Info Screen should display
    And Enter First name as "Manoj"
    And Enter Last name as "Ahirwar"
    And Reset the App

